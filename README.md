# autOSINT

This is an automatic osint scanner. It uses a lot of modules from recon-ng and has a modyfied version of OSINT-spy included. Feel free to create modules for this and create a pull request.

## Setup
Place all your manual or previous recon inside the `./import` folder. You can import domains, emails, social media profile URLs and usernames.

Then change the creator name in the `config.sh` file.

## Output
all output will be put in the `./report` folder.

## Start
`$ ./start example.com`
