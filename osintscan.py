from config import *
import json
import argparse
import requests
import clearbit
import os
from fullcontact import FullContact
parser = argparse.ArgumentParser()
parser.add_argument("--email", help="Gather information from email address.")
parser.add_argument("--domain", help="Find out basic detail of particular company through its domain name")
args = parser.parse_args()
fc=FullContact(fullcontact_api_key)
clearbit.key=clearbit_api_key

def fetchData(email_id):
    person = fc.person(email=email_id)
    data = person.json()
    return data

def getDetail(domain):
    try:
        company=clearbit.Company.find(domain=domain,stream=False)
        if company != None:
            url="https://api.hunter.io/v2/domain-search?domain="+domain+"&api_key="+email_hunter_api_key
            emails=requests.get(url)
            parsed_emails=json.loads(emails.text)
            company["parsed_emails"] = parsed_emails
            return company
        else:
            print("No information")
    except:
        print("Try Again")

if args.email:
    email_id=args.email
    emaildata=fetchData(email_id)
    print(emaildata)

if args.domain:
    domain=args.domain
    companydata=getDetail(domain)
    print(companydata)
