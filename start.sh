#!/bin/bash
if [ -n "$1" ]; then
  echo "Starting autOSINT"
else
  echo -e "Usage: ./start example.com"
  exit
fi
source config.sh
mkdir report temp 2>/dev/null

source modules/contactGrabber.sh
source modules/autoReconCli.sh

rm -rf temp

# notify
