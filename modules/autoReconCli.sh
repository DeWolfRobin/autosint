recon-cli -w $company -m "import/list" -o COLUMN="domain" -o TABLE="domains" -o FILENAME=$domainfile -x
recon-cli -w $company -m "import/list" -o COLUMN="email" -o TABLE="contacts" -o FILENAME=$emailfile -x
recon-cli -w $company -m "import/list" -o COLUMN="username" -o TABLE="profiles" -o FILENAME=$usernames -x
recon-cli -w $company -m "import/list" -o COLUMN="url" -o TABLE="profiles" -o FILENAME=$profurls -x
recon-cli -w $company -m "recon/domains-hosts/findsubdomains" -x
recon-cli -w $company -m "discovery/info_disclosure/interesting_files" -x
# recon-cli -w $company -m "recon/companies-contacts/bing_linkedin_cache" -o SOURCE=$search -x
recon-cli -w $company -m "recon/companies-domains/pen" -x
recon-cli -w $company -m "recon/companies-contacts/pen" -x
recon-cli -w $company -m "recon/companies-multi/github_miner" -x
recon-cli -w $company -m "recon/companies-multi/whois_miner" -o SOURCE=$company -x
# recon-cli -w $company -m "recon/contacts-contacts/mailtester" -o REMOVE=$remove -x
recon-cli -w $company -m "recon/contacts-credentials/hibp_breach" -x
recon-cli -w $company -m "recon/contacts-credentials/hibp_paste" -x
recon-cli -w $company -m "recon/contacts-domains/migrate_contacts" -x
recon-cli -w $company -m "recon/contacts-profiles/fullcontact" -x
recon-cli -w $company -m "recon/credentials-credentials/adobe" -x
recon-cli -w $company -m "recon/credentials-credentials/bozocrack" -x
recon-cli -w $company -m "recon/credentials-credentials/hashes_org" -x
recon-cli -w $company -m "recon/domains-companies/pen" -x
recon-cli -w $company -m "recon/domains-contacts/metacrawler" -o EXTRACT="False" -x
recon-cli -w $company -m "recon/domains-contacts/pen" -x
recon-cli -w $company -m "recon/domains-contacts/pgp_search" -x
recon-cli -w $company -m "recon/domains-contacts/whois_pocs" -x
recon-cli -w $company -m "recon/domains-credentials/pwnedlist/account_creds" -x
recon-cli -w $company -m "recon/domains-credentials/pwnedlist/domain_creds" -x
recon-cli -w $company -m "recon/domains-credentials/pwnedlist/domain_ispwned" -x
recon-cli -w $company -m "recon/domains-credentials/pwnedlist/leak_lookup" -x
recon-cli -w $company -m "recon/domains-hosts/brute_hosts" -x
recon-cli -w $company -m "recon/domains-hosts/builtwith" -x
recon-cli -w $company -m "recon/domains-hosts/certificate_transparency" -x
recon-cli -w $company -m "recon/domains-hosts/google_site_web" -x
# recon-cli -w $company -m "recon/domains-hosts/hackertarget" -x
recon-cli -w $company -m "recon/domains-hosts/mx_spf_ip" -x
recon-cli -w $company -m "recon/domains-hosts/netcraft" -x
recon-cli -w $company -m "recon/domains-hosts/shodan_hostname" -x
recon-cli -w $company -m "recon/domains-hosts/ssl_san" -x
recon-cli -w $company -m "recon/domains-hosts/threatcrowd" -x
recon-cli -w $company -m "recon/domains-hosts/threatminer" -x
# recon-cli -w $company -m "recon/domains-vulnerabilities/ghdb" -o GHDB_ADVISORIES_AND_VULNERABILITIES="True" -o GHDB_ERROR_MESSAGES="True" -o GHDB_FILES_CONTAINING_JUICY_INFO="True" -o GHDB_FILES_CONTAINING_PASSWORDS="True" -o GHDB_FILES_CONTAINING_USERNAMES="True" -o GHDB_FOOTHOLDS="True" -o GHDB_NETWORK_OR_VULNERABILITY_DATA="True" -o GHDB_PAGES_CONTAINING_LOGIN_PORTALS="True" -o GHDB_SENSITIVE_DIRECTORIES="True" -o GHDB_SENSITIVE_ONLINE_SHOPPING_INFO="True" -o GHDB_VARIOUS_ONLINE_DEVICES="True" -o GHDB_VULNERABLE_FILES="True" -o GHDB_VULNERABLE_SERVERS="True" -o GHDB_WEB_SERVER_DETECTION="True" -x
# recon-cli -w $company -m "recon/domains-vulnerabilities/punkspider" -x
recon-cli -w $company -m "recon/domains-vulnerabilities/xssed" -x
recon-cli -w $company -m "recon/domains-vulnerabilities/xssposed" -x
recon-cli -w $company -m "recon/hosts-domains/migrate_hosts" -x
# recon-cli -w $company -m "recon/hosts-hosts/ipinfodb" -x
# recon-cli -w $company -m "recon/hosts-hosts/ipstack" -x
recon-cli -w $company -m "recon/hosts-hosts/resolve" -x
recon-cli -w $company -m "recon/hosts-hosts/ssltools" -x
# recon-cli -w $company -m "recon/hosts-hosts/virustotal" -x
recon-cli -w $company -m "recon/hosts-locations/migrate_hosts" -x
recon-cli -w $company -m "recon/hosts-ports/shodan_ip" -x
recon-cli -w $company -m "recon/locations-locations/geocode" -x
recon-cli -w $company -m "recon/locations-locations/reverse_geocode" -x
recon-cli -w $company -m "recon/netblocks-companies/whois_orgs" -x
recon-cli -w $company -m "recon/netblocks-hosts/reverse_resolve" -x
recon-cli -w $company -m "recon/netblocks-hosts/shodan_net" -x
recon-cli -w $company -m "recon/netblocks-hosts/virustotal" -x
recon-cli -w $company -m "recon/netblocks-ports/census_2012" -x
recon-cli -w $company -m "recon/netblocks-ports/censysio" -x
recon-cli -w $company -m "recon/ports-hosts/migrate_ports" -x
recon-cli -w $company -m "recon/profiles-contacts/dev_diver" -x
recon-cli -w $company -m "recon/profiles-contacts/github_users" -x
recon-cli -w $company -m "recon/profiles-profiles/namechk" -x
recon-cli -w $company -m "recon/profiles-profiles/profiler" -x
recon-cli -w $company -m "recon/profiles-repositories/github_repos" -x
recon-cli -w $company -m "recon/repositories-profiles/github_commits" -x
recon-cli -w $company -m "recon/repositories-vulnerabilities/gists_search" -x
recon-cli -w $company -m "recon/repositories-vulnerabilities/github_dorks" -x
recon-cli -w $company -m "reporting/html" -o FILENAME="`pwd`/report/$company.html" -o SANITIZE="False" -o CREATOR="$creator" -o CUSTOMER="$company" -x
recon-cli -w $company -m "reporting/json" -o FILENAME="`pwd`/report/$company.json" -o TABLES="companies,contacts,credentials,domains,hosts,leaks,locations,netblocks,ports,profiles,pushpins,repositories,vulnerabilities" -x
