echo "[" > temp/master.json
while read domain; do
  c=$(python3 osintscan.py --domain $domain)
  echo "$c," | sed "s/'/\"/g"| sed 's/True/"True"/g' | sed 's/False/"False"/g' | sed 's/None/"None"/g' >> temp/master.json
done <import/domains.txt
cat temp/master.json | sed '$ s/,$/]/g' > report/company.json

cat report/company.json | jq -r ".[].parsed_emails.data.emails[].value" >> import/emails.txt
sort import/emails.txt | uniq -u | tee import/emails.txt

echo "[" > temp/master.json
while read email; do
  c=$(python3 osintscan.py --email $email)
  echo "$c," | sed "s/'/\"/g"| sed 's/True/"True"/g' | sed 's/False/"False"/g' | sed 's/None/"None"/g' >> temp/master.json
done <import/emails.txt
cat temp/master.json | sed '$ s/,$/]/g' > report/osint.json
